package cv.me.cv.generator.repo;

import com.fasterxml.jackson.databind.ObjectMapper;
import cv.me.cv.generator.model.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.IOException;

@Component("cvRepo")
public class CvRepo {

    @Autowired
    private ObjectMapper mapper;

    public Data getData() throws IOException {
        return mapper.readValue(new File("src/main/resources/json/cv.json"), Data.class);
    }

}
