package cv.me.cv.generator.model;

import lombok.Getter;
import lombok.ToString;

import java.util.List;

@Getter
@ToString
public class Body {
    private List<Section> sections;
}
