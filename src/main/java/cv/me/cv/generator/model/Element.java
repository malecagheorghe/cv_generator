package cv.me.cv.generator.model;

import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
public class Element {
    private String title;
    private String subtitle;
    private String comment;
    private String description;
}
