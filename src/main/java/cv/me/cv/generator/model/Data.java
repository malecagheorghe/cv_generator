package cv.me.cv.generator.model;

import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
public class Data {
    private PersonalInfo personalInfo;
    private Skills skills;
    private Body body;
}
