package cv.me.cv.generator.model;

import lombok.Getter;
import lombok.ToString;

import java.util.List;

@Getter
@ToString
public class PersonalInfo {
    private String name;
    private String title;
    private String description;
    private List<Contact> contact;
}
