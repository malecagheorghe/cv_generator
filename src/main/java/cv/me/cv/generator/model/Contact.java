package cv.me.cv.generator.model;

import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
public class Contact {
    private String key;
    private String value;
}
