package cv.me.cv.generator;

import cv.me.cv.generator.service.CvService;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import javax.annotation.Resource;

@SpringBootApplication
public class CvGeneratorApplication implements CommandLineRunner {

	@Resource
	CvService cvService;

    public static void main(String[] args) {
        SpringApplication.run(CvGeneratorApplication.class, args);
    }


    @Override
    public void run(String... args) throws Exception {
		cvService.generateCv();
    }
}
