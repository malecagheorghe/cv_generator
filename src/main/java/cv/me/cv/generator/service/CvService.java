package cv.me.cv.generator.service;

import com.itextpdf.html2pdf.ConverterProperties;
import com.itextpdf.html2pdf.HtmlConverter;
import com.itextpdf.kernel.geom.PageSize;
import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.PdfWriter;
import com.itextpdf.layout.Document;
import com.itextpdf.layout.element.AreaBreak;
import com.itextpdf.layout.element.IBlockElement;
import cv.me.cv.generator.model.Data;
import cv.me.cv.generator.model.Element;
import cv.me.cv.generator.repo.CvRepo;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class CvService {

    private final String FILE = "cv.pdf";

    private final String FIRST_PAGE = "src/main/resources/html/first_page.html";
    private final String CONTACT_INFO = "src/main/resources/html/contact_info.html";
    private final String CARD = "src/main/resources/html/card.html";
    private final String CARD_CONTENT = "src/main/resources/html/card_content.html";
    private final String CARD_SEPARATOR = "src/main/resources/html/card_separator.html";

    private final String CONTENT_PAGES = "src/main/resources/html/content_pages.html";
    private final String SECTION = "src/main/resources/html/section.html";
    private final String SECTION_CONTENT = "src/main/resources/html/section_content.html";
    private final String SECTION_SEPARATOR = "src/main/resources/html/section_separator.html";

    private final String IMG = "src/main/resources/img/img.jpg";

    private String cardTemplate;
    private String cardContenTemplate;
    private String cardSeparatorTemplate;
    private String contactInfoTemplate;
    private String contentPagesTemplate;
    private String firstPageTemplate;
    private String sectionTemplate;
    private String sectionContentTemplate;
    private String sectionSeparatorTemplate;
    @Resource(name = "cvRepo")
    private CvRepo cvRepo;
    private Data JSON_DATA;

    public void generateCv() {
        try {
            JSON_DATA = cvRepo.getData();
        } catch (IOException exception) {
            System.out.println("Could not open the following file: src/main/resources/json/cv.json");
            return;
        }
        loadTemplates();

        String firstPage = dynamicBuildFirstPage();
        String contentPages = dynamicBuildContentPages();

        mapToPdf(firstPage, contentPages);
    }

    private void mapToPdf(String firstPage, String contentPages) {

        // build the pdf document
        ConverterProperties properties = new ConverterProperties();
        properties.setBaseUri(IMG);
        PdfWriter writer;

        try {
            writer = new PdfWriter(FILE);
        } catch (FileNotFoundException e) {
            System.out.println(String.format("Could not open the following file: %s", FILE));
            return;
        }

        PdfDocument pdfDoc = new PdfDocument(writer);
        pdfDoc.setDefaultPageSize(PageSize.A4);
        Document document = new Document(pdfDoc);

        // adds the first page to the document
        HtmlConverter.convertToElements(firstPage, properties).forEach(e -> document.add((IBlockElement) e));

        // moves the next elements to a new page
        document.add(new AreaBreak());

        // adds the content from the second page onwards
        HtmlConverter.convertToElements(contentPages).forEach(e -> document.add((IBlockElement) e));

        document.close();
    }

    private String dynamicBuildFirstPage() {
        String firstPageWithPersonalInfo = buildPersonalInfoBlock(firstPageTemplate);
        String skills = buildSkillsBlock();
        return firstPageWithPersonalInfo.replace("{{cards}}", skills);
    }

    private String buildPersonalInfoBlock(String firstPageTemplate) {
        return firstPageTemplate.replace("{{img_path}}", "../img/img.jpg")
            .replace("{{home_name}}", JSON_DATA.getPersonalInfo().getName())
            .replace("{{home_title}}", JSON_DATA.getPersonalInfo().getTitle())
            .replace("{{home_description}}", JSON_DATA.getPersonalInfo().getDescription())
            .replace("{{contact_info}}", buildContactInfo());
    }

    private String buildSkillsBlock() {
        List<String> cardList = buildIndividualSectionsInSkillsBlock();
        addSeparatorsBetweenSectionsInSkillsBlock(cardList);
        return cardList.stream().reduce("", String::concat);
    }

    private void addSeparatorsBetweenSectionsInSkillsBlock(List<String> cardList) {
        for (int i = 0; i < cardList.size() - 1; i++) {
            cardList.set(i, cardList.get(i).concat(cardSeparatorTemplate));
        }
    }

    private List<String> buildIndividualSectionsInSkillsBlock() {
        return JSON_DATA
            .getSkills()
            .getClusters()
            .stream()
            .map(c -> {
                String cardContentsString = c
                    .getContent()
                    .stream()
                    .map(this::buildSkillTitleWithLevel).reduce("", String::concat);

                return cardTemplate.replace("{{card_title}}", c.getTitle())
                    .replace("{{card_content}}", cardContentsString);
            })
            .collect(Collectors.toList());
    }

    private String buildSkillTitleWithLevel(Element content) {
        return cardContenTemplate.replace("{{card_content_subtitle}}",
            (content.getSubtitle() != null && !content.getSubtitle().isBlank())
                ? String.format("( %s )", content.getSubtitle())
                : "")
            .replace("{{card_content_title}}",
                (content.getComment() == null || content.getComment().isBlank())
                    ? content.getTitle()
                    : String.format("<a href=\"%s\">%s</a>", content.getComment(), content.getTitle())
            );
    }

    private String buildContactInfo() {
        return JSON_DATA.getPersonalInfo()
            .getContact()
            .stream()
            .map(contact -> {
                return contactInfoTemplate
                    .replace("{{key}}", contact.getKey())
                    .replace("{{value}}", contact.getValue());
            })
            .reduce("", String::concat);
    }

    private String dynamicBuildContentPages() {
        // build all the sections
        List<String> sections = JSON_DATA
            .getBody()
            .getSections()
            .stream()
            .map(this::buildContentSection)
            .collect(Collectors.toList());

        // add separators between different sections
        separateSections(sections);

        // merge all the sections in one string
        String sectionsString = sections.stream().reduce("", String::concat);

        return contentPagesTemplate.replace("{{sections}}", sectionsString);
    }

    private String buildContentSection(cv.me.cv.generator.model.Section s) {
        String sectionContents = s
            .getContent()
            .stream()
            .map(this::buildSectionContent)
            .reduce("", String::concat);
        String sectionWithTitle = sectionTemplate.replace("{{section_title}}", s.getTitle());
        return sectionWithTitle.replace("{{section_content}}", sectionContents);
    }

    private String buildSectionContent(Element sectionContent) {
        return sectionContentTemplate
            .replace("{{section_content_title}}", sectionContent.getTitle())
            .replace("{{section_content_subtitle}}", sectionContent.getSubtitle())
            .replace("{{section_content_comment}}", sectionContent.getComment())
            .replace("{{section_content_description}}", sectionContent.getDescription());
    }

    private void separateSections(List<String> sections) {
        for (int i = 0; i < sections.size() - 1; i++) {
            sections.set(i, sections.get(i).concat(sectionSeparatorTemplate));
        }
    }

    private void loadTemplates() {
        try {
            cardTemplate = Files.readString(Path.of(CARD));
        } catch (IOException exception) {
            cardTemplate = "";
            System.out.println(String.format("Could not open the following file: %s", CARD));
        }
        try {
            cardContenTemplate = Files.readString(Path.of(CARD_CONTENT));
        } catch (IOException exception) {
            cardContenTemplate = "";
            System.out.println(String.format("Could not open the following file: %s", CARD_CONTENT));
        }
        try {
            cardSeparatorTemplate = Files.readString(Path.of(CARD_SEPARATOR));
        } catch (IOException exception) {
            cardSeparatorTemplate = "";
            System.out.println(String.format("Could not open the following file: %s", CARD_SEPARATOR));
        }
        try {
            contactInfoTemplate = Files.readString(Path.of(CONTACT_INFO));
        } catch (IOException exception) {
            contactInfoTemplate = "";
            System.out.println(String.format("Could not open the following file: %s", CONTACT_INFO));
        }
        try {
            contentPagesTemplate = Files.readString(Path.of(CONTENT_PAGES));
        } catch (IOException exception) {
            contentPagesTemplate = "";
            System.out.println(String.format("Could not open the following file: %s", CONTENT_PAGES));
        }
        try {
            firstPageTemplate = Files.readString(Path.of(FIRST_PAGE));
        } catch (IOException exception) {
            firstPageTemplate = "";
            System.out.println(String.format("Could not open the following file: %s", FIRST_PAGE));
        }
        try {
            sectionTemplate = Files.readString(Path.of(SECTION));
        } catch (IOException exception) {
            sectionTemplate = "";
            System.out.println(String.format("Could not open the following file: %s", SECTION));
        }
        try {
            sectionContentTemplate = Files.readString(Path.of(SECTION_CONTENT));
        } catch (IOException exception) {
            sectionContentTemplate = "";
            System.out.println(String.format("Could not open the following file: %s", SECTION_CONTENT));
        }
        try {
            sectionSeparatorTemplate = Files.readString(Path.of(SECTION_SEPARATOR));
        } catch (IOException exception) {
            sectionSeparatorTemplate = "";
            System.out.println(String.format(" Could not open the following file: %s", SECTION_SEPARATOR));
        }
    }
}
