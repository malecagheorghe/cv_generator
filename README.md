# CV_generator

Generate your CV from a JSON file which contains the information you want to insert in the template.


1. Replace `resources/img/img.jpg` with your picture
2. Change the content of `cv.json` from `resources/json` with the information from your CV
3. Run the program and a file `cv.pdf` will be generated in the root folder of the project
4. Happy job hunting

* If you do not have an internet connetion while running the program the result will have a different styling because the template uses a few classes from bootstrap which are fetched through internet
* For the `sideBar.clusters.content`, if you pass a value to the `comment` attribute, it will be interpreted as an URL and the title will become a link
* All the data from the `JSON` file will be inserted in an `HTML` template before being parsed to the `PDF` format, therefore you can even write `HTML tags` there such as:
`<a href=\"desired_url\">Content which will be seen in the CV</a>`